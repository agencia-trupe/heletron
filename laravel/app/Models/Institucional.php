<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Institucional extends Model
{
    protected $table = 'institucional';

    protected $guarded = ['id'];

    public static function upload_background_empresa()
    {
        return CropImage::make('background_empresa', [
            'width'  => 1980,
            'height' => 400,
            'path'   => 'assets/img/institucional/'
        ]);
    }

    public static function upload_background_missao()
    {
        return CropImage::make('background_missao', [
            'width'  => 1980,
            'height' => 400,
            'path'   => 'assets/img/institucional/'
        ]);
    }

    public static function upload_background_visao()
    {
        return CropImage::make('background_visao', [
            'width'  => 1980,
            'height' => 400,
            'path'   => 'assets/img/institucional/'
        ]);
    }

    public static function upload_background_valores()
    {
        return CropImage::make('background_valores', [
            'width'  => 1980,
            'height' => 400,
            'path'   => 'assets/img/institucional/'
        ]);
    }

}
