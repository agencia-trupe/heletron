<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class ServicoImagem extends Model
{
    protected $table = 'servicos_imagens';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeServico($query, $id)
    {
        return $query->where('servico_id', $id);
    }

    public static function uploadImagem()
    {
        return CropImage::make('imagem', [
            [
                'width'   => 180,
                'height'  => 180,
                'path'    => 'assets/img/servicos/imagens/thumbs/'
            ],
            [
                'width'   => 450,
                'height'  => 450,
                'path'    => 'assets/img/servicos/imagens/'
            ]
        ]);
    }
}
