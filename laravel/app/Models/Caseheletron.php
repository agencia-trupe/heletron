<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Caseheletron extends Model
{
    protected $table = 'cases';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'   => 215,
            'height'  => 120,
            'bgcolor' => '#fff',
            'path'    => 'assets/img/cases/'
        ]);
    }
}
