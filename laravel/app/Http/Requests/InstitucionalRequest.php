<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class InstitucionalRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'empresa' => 'required',
            'background_empresa' => 'image',
            'missao' => 'required',
            'background_missao' => 'image',
            'visao' => 'required',
            'background_visao' => 'image',
            'valores' => 'required',
            'background_valores' => 'image',
        ];
    }
}
