<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('institucional/{pagina_slug?}', 'InstitucionalController@index')->name('institucional');
    Route::get('servicos/{servicos_slug?}', 'ServicosController@index')->name('servicos');
    Route::get('clientes', 'ClientesController@index')->name('clientes');
    Route::get('cases', 'CasesController@index')->name('cases');
    Route::get('parceiros', 'ParceirosController@index')->name('parceiros');
    Route::get('contato', 'ContatoController@index')->name('contato');
    Route::post('contato', 'ContatoController@post')->name('contato.post');


    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
		Route::resource('cases', 'CasesController');
		Route::resource('parceiros', 'ParceirosController');
		Route::resource('clientes', 'ClientesController');
		Route::resource('servicos', 'ServicosController');
		Route::get('servicos/{servicos}/imagens/clear', [
			'as'   => 'painel.servicos.imagens.clear',
			'uses' => 'ServicosImagensController@clear'
		]);
		Route::resource('servicos.imagens', 'ServicosImagensController', ['parameters' => ['imagens' => 'imagens_servicos']]);
		Route::resource('institucional', 'InstitucionalController', ['only' => ['index', 'update']]);
		Route::resource('destaques', 'DestaquesController');
		Route::resource('equipamentos', 'EquipamentosController');
		Route::resource('banners', 'BannersController');
		Route::resource('configuracoes', 'ConfiguracoesController', ['only' => ['index', 'update']]);

        Route::get('contato/recebidos/{recebidos}/toggle', ['as' => 'painel.contato.recebidos.toggle', 'uses' => 'ContatosRecebidosController@toggle']);
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato', 'ContatoController');
        Route::resource('usuarios', 'UsuariosController');

        Route::post('ckeditor-upload', 'PainelController@imageUpload');
        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

        Route::get('generator', 'GeneratorController@index')->name('generator.index');
        Route::post('generator', 'GeneratorController@submit')->name('generator.submit');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
