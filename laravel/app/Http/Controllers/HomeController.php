<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Banner;
use App\Models\Equipamento;
use App\Models\Servico;
use App\Models\Destaque;
use App\Models\Caseheletron;

class HomeController extends Controller
{
    public function index()
    {
        $banners      = Banner::ordenados()->get();
        $equipamentos = Equipamento::ordenados()->get();
        $servicos     = Servico::ordenados()->get();
        $destaques    = Destaque::ordenados()->get();
        $cases        = Caseheletron::orderByRaw('RAND()')->take(4)->get();

        return view('frontend.home', compact('banners', 'equipamentos', 'servicos', 'destaques', 'cases'));
    }
}
