<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Servico;

class ServicosController extends Controller
{
    public function index(Servico $servico)
    {
        if (!$servico->exists) {
            $servico = Servico::ordenados()->first() ?: abort('404');
        }

        $servicos = Servico::ordenados()->lists('titulo', 'slug');

        return view('frontend.servicos', compact('servicos', 'servico'));
    }
}
