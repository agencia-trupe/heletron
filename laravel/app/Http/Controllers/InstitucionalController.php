<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Institucional;

class InstitucionalController extends Controller
{
    public function index($slug = 'a-empresa')
    {
        $slugs = ['a-empresa', 'missao', 'visao', 'valores'];

        if (!in_array($slug, $slugs)) abort('404');

        $institucional = Institucional::first();

        return view('frontend.institucional', compact('slug', 'institucional'));
    }
}
