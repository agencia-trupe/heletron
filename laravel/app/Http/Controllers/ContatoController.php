<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\ContatosRecebidosRequest;
use App\Models\ContatoRecebido;

class ContatoController extends Controller
{
    public function index()
    {
        return view('frontend.contato');
    }

    public function post(ContatosRecebidosRequest $request, ContatoRecebido $contatoRecebido)
    {
        $input = $request->all();

        $contatoRecebido->create($input);

        if ($input['setor'] == 'Departamento Comercial') {
            $emailEnvio = 'comercial@heletron.com.br';
        } elseif ($input['setor'] == 'Departamento Financeiro') {
            $emailEnvio = 'financeiro@heletron.com.br';
        } elseif ($input['setor'] == 'Recrutamento e Seleção') {
            $emailEnvio = 'rh@heletron.com.br';
        } else {
            $emailEnvio = 'heletron@heletron.com.br';
        }

        \Mail::send('emails.contato', $input, function($message) use ($request, $emailEnvio)
        {
            $message->to($emailEnvio, 'Heletron')
                    ->subject('[CONTATO] Heletron')
                    ->replyTo($request->get('email'), $request->get('nome'));
        });

        $response = [
            'status'  => 'success',
            'message' => 'Mensagem enviada com sucesso!'
        ];

        return response()->json($response);
    }
}
