<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\InstitucionalRequest;
use App\Http\Controllers\Controller;

use App\Models\Institucional;

class InstitucionalController extends Controller
{
    public function index()
    {
        $registro = Institucional::first();

        return view('painel.institucional.edit', compact('registro'));
    }

    public function update(InstitucionalRequest $request, Institucional $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['background_empresa'])) $input['background_empresa'] = Institucional::upload_background_empresa();
            if (isset($input['background_missao'])) $input['background_missao'] = Institucional::upload_background_missao();
            if (isset($input['background_visao'])) $input['background_visao'] = Institucional::upload_background_visao();
            if (isset($input['background_valores'])) $input['background_valores'] = Institucional::upload_background_valores();

            $registro->update($input);

            return redirect()->route('painel.institucional.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
