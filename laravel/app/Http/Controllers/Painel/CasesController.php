<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\CasesRequest;
use App\Http\Controllers\Controller;

use App\Models\Caseheletron;

class CasesController extends Controller
{
    public function index()
    {
        $registros = Caseheletron::ordenados()->get();

        return view('painel.cases.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.cases.create');
    }

    public function store(CasesRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Caseheletron::upload_imagem();

            Caseheletron::create($input);

            return redirect()->route('painel.cases.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Caseheletron $registro)
    {
        return view('painel.cases.edit', compact('registro'));
    }

    public function update(CasesRequest $request, Caseheletron $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Caseheletron::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.cases.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Caseheletron $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.cases.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
