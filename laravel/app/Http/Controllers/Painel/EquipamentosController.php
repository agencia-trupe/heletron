<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\EquipamentosRequest;
use App\Http\Controllers\Controller;

use App\Models\Equipamento;

class EquipamentosController extends Controller
{
    public function index()
    {
        $registros = Equipamento::ordenados()->get();

        return view('painel.equipamentos.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.equipamentos.create');
    }

    public function store(EquipamentosRequest $request)
    {
        try {

            $input = $request->all();

            Equipamento::create($input);

            return redirect()->route('painel.equipamentos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Equipamento $registro)
    {
        return view('painel.equipamentos.edit', compact('registro'));
    }

    public function update(EquipamentosRequest $request, Equipamento $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.equipamentos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Equipamento $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.equipamentos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
