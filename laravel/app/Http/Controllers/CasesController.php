<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Caseheletron;

class CasesController extends Controller
{
    public function index()
    {
        $cases = Caseheletron::ordenados()->get();

        return view('frontend.cases', compact('cases'));
    }
}
