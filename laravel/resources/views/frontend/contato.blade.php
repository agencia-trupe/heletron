@extends('frontend.common.template')

@section('content')

    <div class="contato">
        <div class="mapa">{!! $contato->google_maps !!}</div>
        <div class="center">
            <div class="box">
                <h2>CONTATO</h2>
                <div class="social">
                    @foreach(['facebook', 'instagram'] as $s)
                    @if($contato->{$s})
                        <a href="{{ $contato->{$s} }}" class="{{ $s }}" target="_blank">{{ $s }}</a>
                    @endif
                    @endforeach
                </div>
                <p class="telefones">
                    @foreach(explode(',', $contato->telefones) as $telefone)
                    <span>{{ trim($telefone) }}</span>
                    @endforeach
                </p>
                <p class="endereco">{!! $contato->endereco !!}</p>
                <p class="emails">
                    @foreach(explode(',', $contato->emails) as $email)
                    <a href="mailto:{{ trim($email) }}">{{ $email }}</a>
                    @endforeach
                </p>
            </div>

            <form action="" id="form-contato" method="POST">
                <select name="setor" id="setor" required>
                    <option value="">Setor [selecione...]</option>
                    <option value="Contato Principal">Contato Principal</option>
                    <option value="Departamento Comercial">Departamento Comercial</option>
                    <option value="Departamento Financeiro">Departamento Financeiro</option>
                    <option value="Recrutamento e Seleção">Recrutamento e Seleção</option>
                </select>
                <input type="text" name="nome" id="nome" placeholder="Nome" required>
                <input type="email" name="email" id="email" placeholder="E-mail" required>
                <input type="text" name="telefone" id="telefone" placeholder="Telefone">
                <textarea name="mensagem" id="mensagem" placeholder="Mensagem" required></textarea>
                <input type="submit" value="ENVIAR">
                <div id="form-contato-response"></div>
            </form>
        </div>
    </div>

@endsection
