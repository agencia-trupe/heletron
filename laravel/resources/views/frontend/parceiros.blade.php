@extends('frontend.common.template')

@section('content')

    <div class="parceiros">
        <div class="center">
            <div class="box">
                <h2>PARCEIROS E FORNECEDORES</h2>
                <div class="marcas">
                    @foreach($parceiros as $parceiro)
                    <img src="{{ asset('assets/img/parceiros/'.$parceiro->imagem) }}" alt="">
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
