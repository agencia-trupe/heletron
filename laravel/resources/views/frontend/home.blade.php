@extends('frontend.common.template')

@section('content')

    <div class="home">
        <div class="banners">
            <div class="banners-slides">
                @foreach($banners as $banner)
                <div style="background-image:url({{ asset('assets/img/banners/'.$banner->imagem) }})" data-link="{{ $banner->link }}" data-titulo="{{ $banner->titulo }}" data-texto="{{ $banner->texto }}"></div>
                @endforeach
            </div>
            <div class="chamada">
                <div class="center">
                    @if(count($banners))
                    <div class="texto">
                        <h3>{{ $banners->first()->titulo }}</h3>
                        <p>{{ $banners->first()->texto }}</p>
                    </div>
                    <a href="{{ $banners->first()->link }}" class="link">SAIBA MAIS &raquo;</a>
                    <div id="cycle-pager"></div>
                    @endif
                </div>
            </div>
        </div>

        <div class="servicos-home">
            <div class="center">
                <h2>CONHEÇA NOSSOS SERVIÇOS</h2>
                <div class="lista">
                    @foreach($servicos as $servico)
                    <a href="{{ route('servicos', $servico->slug) }}">
                        &raquo; {{ $servico->titulo }}
                    </a>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="equipamentos">
            <div class="center">
                <h2>PRINCIPAIS EQUIPAMENTOS FORNECIDOS</h2>
                <div class="lista">
                    @foreach($equipamentos as $equipamento)
                    <span>{{ $equipamento->titulo }}</span>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="destaques">
            <div class="center">
                <h2>PRINCIPAIS DESTAQUES DA NOSSA ATUAÇÃO</h2>
                <div class="lista">
                    @foreach($destaques as $destaque)
                    <div class="destaque">
                        <img src="{{ asset('assets/img/destaques/'.$destaque->imagem) }}" alt="">
                        <h3>{{ $destaque->titulo }}</h3>
                        <div class="texto">
                            {!! $destaque->descricao !!}
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="cases-home">
            <div class="center">
                <h3>CONFIRA ALGUNS DOS NOSSOS CASES:</h3>
                <div class="lista">
                    @foreach($cases as $case)
                    <a href="{{ route('cases') }}">
                        <img src="{{ asset('assets/img/cases/'.$case->imagem) }}" alt="">
                    </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
