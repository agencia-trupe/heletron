@extends('frontend.common.template')

@section('content')

    <div class="clientes">
        <div class="center">
            <div class="box">
                <h2>CLIENTES</h2>
                <div class="marcas">
                    @foreach($clientes as $cliente)
                    <img src="{{ asset('assets/img/clientes/'.$cliente->imagem) }}" alt="">
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
