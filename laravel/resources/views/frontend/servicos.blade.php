@extends('frontend.common.template')

@section('content')

    <div class="servicos">
        <div class="center">
            <div class="box">
                <div class="titulo">
                    <h2>SERVIÇOS</h2>
                    <?php
                        $servicos = array_map(function($item) {
                            return '&raquo; '.$item;
                        }, $servicos->toArray());
                    ?>
                    {!! Form::select('servico', $servicos, $servico->slug) !!}
                </div>

                <div class="texto">
                    <h3>{{ $servico->titulo }}</h3>
                    {!! $servico->descricao !!}
                </div>

                @if(count($servico->imagens))
                <div class="imagens">
                    @foreach($servico->imagens as $imagem)
                    <img src="{{ asset('assets/img/servicos/imagens/'.$imagem->imagem) }}" alt="">
                    @endforeach
                    <div class="controls">
                        <span class="cycle-prev">&laquo;</span>
                        <span class="cycle-next">&raquo;</span>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>

@endsection
