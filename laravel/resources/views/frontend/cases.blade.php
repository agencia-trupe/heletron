@extends('frontend.common.template')

@section('content')

    <div class="cases">
        <div class="center">
            <div class="box">
                <h2>CASES</h2>
                <div class="cases-lista">
                    @foreach($cases as $case)
                    <p>
                        <strong>{{ $case->titulo }}</strong>
                        {{ $case->descricao }}
                    </p>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
