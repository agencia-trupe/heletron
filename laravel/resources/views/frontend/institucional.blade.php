@extends('frontend.common.template')

@section('content')

    <div class="institucional">
        <div class="institucional-nav">
            <div class="center">
                <a href="{{ route('institucional', 'a-empresa') }}" @if($slug == 'a-empresa') class="active" @endif>&raquo; a empresa</a>
                <a href="{{ route('institucional', 'missao') }}" @if($slug == 'missao') class="active" @endif>&raquo; missão</a>
                <a href="{{ route('institucional', 'visao') }}" @if($slug == 'visao') class="active" @endif>&raquo; visão</a>
                <a href="{{ route('institucional', 'valores') }}" @if($slug == 'valores') class="active" @endif>&raquo; valores</a>
            </div>
        </div>
        <div class="banner" style="background-image:url({{ asset('assets/img/institucional/'.$institucional->{'background_'.str_replace('a-', '', $slug)}) }})"></div>
        <div class="center">
            <div class="box box-{{ $slug }}">
                @if($slug == 'a-empresa')
                <h2>A EMPRESA</h2>
                @elseif($slug == 'missao')
                <h2>MISSÃO</h2>
                @elseif($slug == 'visao')
                <h2>VISÃO</h2>
                @elseif($slug == 'valores')
                <h2>VALORES</h2>
                @endif

                {!! $institucional->{str_replace('a-', '', $slug)} !!}
            </div>
        </div>
    </div>

@endsection
