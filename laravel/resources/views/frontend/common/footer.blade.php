    <footer>
        <div class="center">
            <div class="col">
                <a href="{{ route('home') }}" class="title">HOME</a>
                <a href="{{ route('institucional') }}" class="title">INSTITUCIONAL</a>
                <a href="{{ route('institucional', 'a-empresa') }}">&middot; a empresa</a>
                <a href="{{ route('institucional', 'missao') }}">&middot; missão</a>
                <a href="{{ route('institucional', 'visao') }}">&middot; visão</a>
                <a href="{{ route('institucional', 'valores') }}">&middot; valores</a>
            </div>
            <div class="col">
                <a href="{{ route('servicos') }}" class="title">SERVIÇOS</a>
                @foreach($servicosFooter as $servico)
                <a href="{{ route('servicos', $servico->slug) }}">&middot; {{ $servico->titulo }}</a>
                @endforeach
            </div>
            <div class="col">
                <a href="{{ route('clientes') }}" class="title">CLIENTES</a>
                <a href="{{ route('cases') }}" class="title">CASES</a>
                <a href="{{ route('parceiros') }}" class="title">PARCEIROS</a>
                <a href="{{ route('contato') }}" class="title">CONTATO</a>
                <p class="telefones">
                    @foreach(explode(',', $contato->telefones) as $telefone)
                    <span>{{ trim($telefone) }}</span>
                    @endforeach
                </p>
                <p class="endereco">{!! $contato->endereco !!}</p>
            </div>
            <div class="col">
                <img src="{{ asset('assets/img/layout/heletron-footer.png') }}" alt="">
                <p class="copyright">
                    © {{ date('Y') }} {{ $config->nome_do_site }}<br>
                    Todos os direitos reservados.<br><br>
                    <a href="http://www.trupe.net" target="_blank">Criação de sites</a>:<br>
                    <a href="http://www.trupe.net" target="_blank">Trupe Agência Criativa</a>
                </p>
            </div>
        </div>
    </footer>
