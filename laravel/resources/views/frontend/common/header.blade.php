    <header>
        <div class="center">
            <a href="{{ route('home') }}" class="logo">{{ $config->nome_do_site }}</a>
            <nav id="nav-desktop">
                @include('frontend.common.nav')
            </nav>
            <button id="mobile-toggle" type="button" role="button">
                <span class="lines"></span>
            </button>
            <div class="social">
                @foreach(['facebook', 'instagram'] as $s)
                @if($contato->{$s})
                    <a href="{{ $contato->{$s} }}" class="{{ $s }}" target="_blank">{{ $s }}</a>
                @endif
                @endforeach
            </div>
        </div>

        <nav id="nav-mobile">
            @include('frontend.common.nav')
        </nav>
    </header>
