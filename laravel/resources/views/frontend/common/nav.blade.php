<a href="{{ route('home') }}" @if(Tools::isActive('home')) class="active" @endif>Home</a>
<a href="{{ route('institucional') }}" @if(Tools::isActive('institucional')) class="active" @endif>Institucional</a>
<a href="{{ route('servicos') }}" @if(Tools::isActive('servicos')) class="active" @endif>Serviços</a>
<a href="{{ route('clientes') }}" @if(Tools::isActive('clientes')) class="active" @endif>Clientes</a>
<a href="{{ route('cases') }}" @if(Tools::isActive('cases')) class="active" @endif>Cases</a>
<a href="{{ route('parceiros') }}" @if(Tools::isActive('parceiros')) class="active" @endif>Parceiros</a>
<a href="{{ route('contato') }}" @if(Tools::isActive('contato')) class="active" @endif>Contato</a>
<p>
    <span>Consultores e Equipe Técnica:</span><br>
    Grande São Paulo e Baixada Santista<br>
    Região de Campinas<br>
    Região de Piracicaba<br>
    Região Metropolitana da Cidade do Rio de janeiro<br>
</p>
