@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Cases /</small> Adicionar Case</h2>
    </legend>

    {!! Form::open(['route' => 'painel.cases.store', 'files' => true]) !!}

        @include('painel.cases.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
