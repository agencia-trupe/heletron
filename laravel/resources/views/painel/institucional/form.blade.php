@include('painel.common.flash')

<div class="row">
    <div class="col-md-8">
        <div class="form-group">
            {!! Form::label('empresa', 'Empresa') !!}
            {!! Form::textarea('empresa', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('background_empresa', 'Background Empresa') !!}
            <img src="{{ url('assets/img/institucional/'.$registro->background_empresa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('background_empresa', ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-md-8">
        <div class="form-group">
            {!! Form::label('missao', 'Missão') !!}
            {!! Form::textarea('missao', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('background_missao', 'Background Missão') !!}
            <img src="{{ url('assets/img/institucional/'.$registro->background_missao) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('background_missao', ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-md-8">
        <div class="form-group">
            {!! Form::label('visao', 'Visão') !!}
            {!! Form::textarea('visao', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('background_visao', 'Background Visão') !!}
            <img src="{{ url('assets/img/institucional/'.$registro->background_visao) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('background_visao', ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-md-8">
        <div class="form-group">
            {!! Form::label('valores', 'Valores') !!}
            {!! Form::textarea('valores', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('background_valores', 'Background Valores') !!}
            <img src="{{ url('assets/img/institucional/'.$registro->background_valores) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('background_valores', ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
