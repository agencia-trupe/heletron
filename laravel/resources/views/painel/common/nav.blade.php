<ul class="nav navbar-nav">
    <li class="dropdown @if(str_is('painel.banners*', Route::currentRouteName()) || str_is('painel.equipamentos*', Route::currentRouteName()) || str_is('painel.destaques*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Home
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(str_is('painel.banners*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.banners.index') }}">Banners</a>
            </li>
            <li @if(str_is('painel.equipamentos*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.equipamentos.index') }}">Equipamentos</a>
            </li>
            <li @if(str_is('painel.destaques*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.destaques.index') }}">Destaques</a>
            </li>
        </ul>
    </li>
	<li @if(str_is('painel.institucional*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.institucional.index') }}">Institucional</a>
	</li>
    <li @if(str_is('painel.servicos*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.servicos.index') }}">Serviços</a>
    </li>
    <li @if(str_is('painel.clientes*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.clientes.index') }}">Clientes</a>
    </li>
    <li @if(str_is('painel.cases*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.cases.index') }}">Cases</a>
    </li>
    <li @if(str_is('painel.parceiros*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.parceiros.index') }}">Parceiros</a>
    </li>
    <li class="dropdown @if(str_is('painel.contato*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Contato
            @if($contatosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li><a href="{{ route('painel.contato.index') }}">Informações de Contato</a></li>
            <li><a href="{{ route('painel.contato.recebidos.index') }}">
                Contatos Recebidos
                @if($contatosNaoLidos >= 1)
                <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
                @endif
            </a></li>
        </ul>
    </li>
</ul>
