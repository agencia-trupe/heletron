@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Equipamentos /</small> Adicionar Equipamento</h2>
    </legend>

    {!! Form::open(['route' => 'painel.equipamentos.store', 'files' => true]) !!}

        @include('painel.equipamentos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
