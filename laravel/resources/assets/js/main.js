import AjaxSetup from './AjaxSetup';
import MobileToggle from './MobileToggle';

AjaxSetup();
MobileToggle();

$('.banners-slides').cycle({
    slides:'>div',
    pager:'#cycle-pager',
    pagerTemplate:'<a href="#">{{slideNum}}</a>'
}).on('cycle-before', function(event, opts, curr, next) {
    $('.chamada .texto h3').text($(next).data('titulo'));
    $('.chamada .texto p').text($(next).data('texto'));
    $('.chamada .link').attr('href', $(next).data('link'));
});

$('.servicos .imagens').cycle({
    next:'>.controls .cycle-next',
    prev:'>.controls .cycle-prev'
});

$('.servicos select').change(function() {
    var slug    = $(this).val(),
        base    = $('base').attr('href');

    if (slug) {
        window.location = `${base}/servicos/${slug}`;
    }
});

$(document).on('submit', '#form-contato', function(event) {
    event.preventDefault();

    var $form     = $(this),
        $response = $('#form-contato-response');

    if ($form.hasClass('sending')) return false;

    $response.fadeOut('fast');
    $form.addClass('sending');

    $.ajax({
        type: "POST",
        url: $('base').attr('href') + '/contato',
        data: {
            setor: $('#setor').val(),
            nome: $('#nome').val(),
            email: $('#email').val(),
            telefone: $('#telefone').val(),
            mensagem: $('#mensagem').val(),
        },
        success: function(data) {
            $response.fadeOut().text(data.message).fadeIn('slow');
            $form[0].reset();
        },
        error: function(data) {
            var error = 'Preencha todos os campos corretamente.';
            $response.fadeOut().text(error).fadeIn('slow');
        },
        dataType: 'json'
    })
    .always(function() {
        $form.removeClass('sending');
    });
});
