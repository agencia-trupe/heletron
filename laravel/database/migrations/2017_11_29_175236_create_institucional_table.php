<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstitucionalTable extends Migration
{
    public function up()
    {
        Schema::create('institucional', function (Blueprint $table) {
            $table->increments('id');
            $table->text('empresa');
            $table->string('background_empresa');
            $table->text('missao');
            $table->string('background_missao');
            $table->text('visao');
            $table->string('background_visao');
            $table->text('valores');
            $table->string('background_valores');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('institucional');
    }
}
