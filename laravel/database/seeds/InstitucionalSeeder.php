<?php

use Illuminate\Database\Seeder;

class InstitucionalSeeder extends Seeder
{
    public function run()
    {
        DB::table('institucional')->insert([
            'empresa' => '',
            'background_empresa' => '',
            'missao' => '',
            'background_missao' => '',
            'visao' => '',
            'background_visao' => '',
            'valores' => '',
            'background_valores' => '',
        ]);
    }
}
