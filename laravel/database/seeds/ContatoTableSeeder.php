<?php

use Illuminate\Database\Seeder;

class ContatoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contato')->insert([
            'emails' => 'comercial@heletron.com.br, rh@heletron.com.br',
        ]);
    }
}
